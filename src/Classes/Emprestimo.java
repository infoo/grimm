/*  _      __     ___    _        ____     _____     ___
 *  (_    _) |    \  |  | \    ___)   )   (     )   (   
 *    |  |   |  |\ \ |  |  |  (__    /     \   /     \  
 *    |  |   |  | \ \|  |  |   __)  (       ) (       ) 
 *   _|  |_  |  |  \    |  |  (      \     /   \     /  
 *  (      )_|  |___\   |_/    \______)   (_____)   (___
 */
package Classes;


public class Emprestimo{

  // Atributos

  private int cod_emprestimo_;
  private int cod_livro_;
  private int cod_usuario_;
  private int cod_funcionario_;
  private String data_emprestimo_;
  private String data_devolucao_;
  private boolean status_emprestimo_;


  // Metodo construtor

  public Emprestimo(){};


  // Metodos de acesso

  public int getCodEmprestimo(){
    return cod_emprestimo_;
  }
  public void setCodEmprestimo(int cod){
    this.cod_emprestimo_ = cod;
  }

  public int getCodLivro(){
    return cod_emprestimo_;
  }
  public void setCodLivro(int cod){
    this.cod_livro_ = cod;
  }

  public String getDataEmprestimo(){
    return data_emprestimo_;
  }
  public void setDataEmprestimo(String data){
    this.data_emprestimo_ = data;
  }

  public String getDataDevolucao(){
    return data_devolucao_;
  }
  public void setDataDevolucao(String data){
    this.data_devolucao_ = data;
  }

  public boolean getStatusEmprestimo(){
    return this.status_emprestimo_;
  }
  public void setStatusEmprestimo(boolean status_emprestimo){
    this.status_emprestimo_ = status_emprestimo;
  }
  

  // Metodos

  public void CadastrarEmprestimo(){
    // ToDo
  }
  public void FinalizarEmprestimo(){
    // ToDo
  }
  
}