/*  _      __     ___    _        ____     _____     ___
 *  (_    _) |    \  |  | \    ___)   )   (     )   (   
 *    |  |   |  |\ \ |  |  |  (__    /     \   /     \  
 *    |  |   |  | \ \|  |  |   __)  (       ) (       ) 
 *   _|  |_  |  |  \    |  |  (      \     /   \     /  
 *  (      )_|  |___\   |_/    \______)   (_____)   (___
 */

package Classes;

public class Usuario{

  // Atributos

  private int cod_usuario_;
  private String data_nasc_;
  private String nome_;
  private String cpf_;
  private String rg_;
  private String endereco_;
  private String email_;
  
  
  // Metodo construtor

  public Usuario(){};

  
  // Metodos de acesso

  public int getCodUsuario(){
    return this.cod_usuario_;
  }
  public void setCodUsuario(int cod){
    this.cod_usuario_ = cod;
  }

  public String getDataNasc(){
    return this.data_nasc_;
  }
  public void setDataNasc(String data){
    this.data_nasc_ = data;
  }

  public String getNome(){
    return this.nome_;
  }
  public void setNome(String nome){
    this.nome_ = nome;
  }

  public String getCpf(){
    return this.cpf_;
  }
  public void setCpf(String cpf){
    this.cpf_ = cpf;
  }

  public String getRg(){
    return this.rg_;
  }
  public void setRg(String rg){
    this.rg_ = rg;
  }

  public String getEndereco(){
    return this.endereco_;
  }
  public void setEndereco(String endereco){
    this.endereco_ = endereco;
  }

  public String getEmail(){
    return this.email_;
  }
  public void setEmail(String email){
    this.email_ = email;
  }


  // Metodos
  
  public void Historico(){
    // ToDo

  }
  public void CadastrarUsuario(){
    // ToDo
  }
  public void EditarUsuario(){
    // ToDo
  }
  public void InativarUsuario(){
    // ToDo
  }
  public void ConsultarUsuario(){
    // ToDo
  }
}