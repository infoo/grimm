/*  _      __     ___    _        ____     _____     ___
 *  (_    _) |    \  |  | \    ___)   )   (     )   (   
 *    |  |   |  |\ \ |  |  |  (__    /     \   /     \  
 *    |  |   |  | \ \|  |  |   __)  (       ) (       ) 
 *   _|  |_  |  |  \    |  |  (      \     /   \     /  
 *  (      )_|  |___\   |_/    \______)   (_____)   (___
 */

package Classes;

public class Reserva{

  // Atributos

  private int cod_reserva_;
  private int cod_livro_;
  private String data_reserva_;
  private String data_final_;
  private String status_livro_;


  // Metodo construtor

  public Reserva(){};


  // Metodos de acesso

  public int getCodReserva(){
    return this.cod_reserva_;
  }
  public void setCodReserva(int cod_reserva){
    this.cod_reserva_ = cod_reserva;
  }

  public int getCodLivro(){
    return this.cod_livro_;
  }
  public void setCodLivro(int cod_livro){
    this.cod_livro_ = cod_livro;
  }

  public String getDataReserva(){
    return this.data_reserva_;
  }
  public void setDataReserva(String data_reserva){
    this.data_reserva_ = data_reserva;
  }

  public String getDataFinal(){
    return this.data_final_;
  }
  public void setDataFinal(String data_final){
    this.data_final_ = data_final;
  }

  public String getStatusLivro(){
    return this.status_livro_;
  }
  public void setStatusLivro(String status_livro){
    this.status_livro_ = status_livro;
  }

  
  // Metodos
  
  public void CadastrarReserva(){
    // ToDO
  }
  public void CancelarReserva(){
    // ToDO
  } 
}