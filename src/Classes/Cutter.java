/*  _      __     ___    _        ____     _____     ___
 *  (_    _) |    \  |  | \    ___)   )   (     )   (   
 *    |  |   |  |\ \ |  |  |  (__    /     \   /     \  
 *    |  |   |  | \ \|  |  |   __)  (       ) (       ) 
 *   _|  |_  |  |  \    |  |  (      \     /   \     /  
 *  (      )_|  |___\   |_/    \______)   (_____)   (___
 */
package Classes;

public class Cutter {
  //Atributos

  private int cod_Cutter;
  private String desc_Cutter;
	
	
  // Metodo Inicializador

  public Cutter(){}


  // Metodos de acesso

  public int getCodCutter(){
  	return cod_Cutter;
  }
  public void setCodCutter(int cod){
  	this.cod_Cutter = cod;
  }

  public String getDescCutter(){
  	return desc_Cutter;
  }
  public void setDescCutter(String desc){
  	this.desc_Cutter = desc;
  }
}