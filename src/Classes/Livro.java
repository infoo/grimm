/*  _      __     ___    _        ____     _____     ___
 *  (_    _) |    \  |  | \    ___)   )   (     )   (   
 *    |  |   |  |\ \ |  |  |  (__    /     \   /     \  
 *    |  |   |  | \ \|  |  |   __)  (       ) (       ) 
 *   _|  |_  |  |  \    |  |  (      \     /   \     /  
 *  (      )_|  |___\   |_/    \______)   (_____)   (___
 */

package Classes;


public class Livro{
  //Atributos

  private int cod_Livro;
  private int cod_Autor;
  private int cod_CDU;
  private String nome;
  private String autor;
  private String edicao;
  private String editora;
	
	
  //Metodo Inicializador

  public Livro(){}


  //Metodos de acesso

  public int getCodLivro(){
  	return cod_Livro;
  }
  public void setCodLivro(int cod){
  	this.cod_Livro = cod;
  }

  public int getCodAutor(){
  	return cod_Autor;
  }
  public void setCodAutor(int cod){
    this.cod_Autor = cod;
  }

  public int getCodCDU(){
  	return cod_CDU;
  }
  public void setCodCDU(int cod){
  	this.cod_CDU = cod;
  }

  public String getNome(){
  	return nome;
  }
  public void setNome(String n){
  	this.nome = n;
  }

  public String getAutor(){
  	return autor;
  }
  public void setAutor(String a){
    this.autor = a;
  }

  public String getEdicao(){
  	return edicao;
  }
  public void setEdicao(String edicao){
    this.edicao = edicao;
  }

  public String getEditora(){
  	return editora;
  }
  public void setEditora(String edit){
  	this.editora = edit;
  }


  //Metodos

  public void CadastrarLivro(){
    //ToDo
  }
  public void EditarLivro(){
    //ToDo
  }
  public void ConsultarLivro(){
    //ToDo
  }
  public boolean StatusLivro(){
    //ToDo
    return true;
  }
}
