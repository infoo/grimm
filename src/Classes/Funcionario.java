/*  _      __     ___    _        ____     _____     ___
 *  (_    _) |    \  |  | \    ___)   )   (     )   (   
 *    |  |   |  |\ \ |  |  |  (__    /     \   /     \  
 *    |  |   |  | \ \|  |  |   __)  (       ) (       ) 
 *   _|  |_  |  |  \    |  |  (      \     /   \     /  
 *  (      )_|  |___\   |_/    \______)   (_____)   (___
 */

package Classes;

public class Funcionario extends Usuario {
  //Atributos

  private String login;
  private String senha;
  private String funcao;


  //Metodo Inicializador

  public Funcionario(){}


  //Metodos de acesso

  public String getLogin(){
    return login;
  }
  public void setLogin(String l){
    this.login = l;
  }

  public String getSenha(){
    return senha;
  }
  public void setSenha(String s){
    this.senha = s;
  }

  public String getFuncao(){
    return funcao;
  }
  public void setFuncao(String f){
    this.funcao = f;
  }


  //Metodos

  public void CadastrarFuncionario(){
    //ToDo
  }
  public void EditarFuncionario(){
    //ToDo
  }
  public void VerificarFuncao(){
    //ToDo
  }
}