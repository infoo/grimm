/*  _      __     ___    _        ____     _____     ___
 *  (_    _) |    \  |  | \    ___)   )   (     )   (   
 *    |  |   |  |\ \ |  |  |  (__    /     \   /     \  
 *    |  |   |  | \ \|  |  |   __)  (       ) (       ) 
 *   _|  |_  |  |  \    |  |  (      \     /   \     /  
 *  (      )_|  |___\   |_/    \______)   (_____)   (___
 */

package Classes;


public class CDU {
  //Atributos
  private int cod_CDU;
  private String descricao;


  //Metodo Inicializador

  public CDU(){}
	
	
  //Metodos de acesso

  public int getCodCDU(){
    return cod_CDU;
  }
  public void setCodCDU(int cod){
    this.cod_CDU = cod;
  }	  
  public String getDescricao(){
    return descricao;
  }
  public void setDescricao(String descricao){
    this.descricao = descricao;
  }


  //Metodos

  public void CadastrarCDU(){
    //ToDo
  }
}