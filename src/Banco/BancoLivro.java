/*  _      __     ___    _        ____     _____     ___
 *  (_    _) |    \  |  | \    ___)   )   (     )   (   
 *    |  |   |  |\ \ |  |  |  (__    /     \   /     \  
 *    |  |   |  | \ \|  |  |   __)  (       ) (       ) 
 *   _|  |_  |  |  \    |  |  (      \     /   \     /  
 *  (      )_|  |___\   |_/    \______)   (_____)   (___
 */

package Banco;

import Classes.Livro;
import com.sun.rowset.CachedRowSetImpl;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class BancoLivro {
  Conexao dados_conexao;
  PreparedStatement prepared_statement;
  CachedRowSetImpl resultado;
  
  
  // Metodo construtor
  public BancoLivro() {
    dados_conexao = null;
    prepared_statement = null;
    resultado = null;
  }
  
  
  
  // Metodos
  public boolean Cadastrar(Livro livro) throws SQLException{
    dados_conexao = new Conexao();
    
    try{
      prepared_statement = dados_conexao.conexao_banco
              .prepareStatement("INSERT INTO " +
              "Livro (cod_livro, titulo, edicao, editora, cutter, cod_cdu) " +
              "VALUES (default, ?, ?, ?, ?, ?)");
      prepared_statement.setString(1, livro.getNome());
      prepared_statement.setByte(2, livro.getEdicao());
      prepared_statement.setString(3, livro.getEditora());
      prepared_statement.setString(4, livro.getCutter());
      prepared_statement.setInt(5, livro.getCodCDU());
      prepared_statement.executeUpdate();
      
      return true;
      
    }catch(SQLException erro_consultar){
      JOptionPane.showMessageDialog(null, "Nao foi possivel cadastrar o livro" +
              "\nErro: " + erro_consultar);
      
      return false;
      
    }finally{
      dados_conexao.Desconectar();
      prepared_statement.close();
    }
  }
  
  public CachedRowSetImpl ConsultarPorCodigo(Livro livro) throws SQLException{
    dados_conexao = new Conexao();
    resultado = new CachedRowSetImpl();
    
    try {
      
      prepared_statement = dados_conexao.conexao_banco
              .prepareStatement("SELECT * FROM Livro " + 
                                "WHERE cod_livro = ?");
      prepared_statement.setInt(1, livro.getCodLivro());
      
      resultado.populate(prepared_statement.executeQuery());
      
    }catch(SQLException erro_consultar){
      JOptionPane.showMessageDialog(null, 
              "Nao foi possivel consultar o livro." +
              "\nErro: " + erro_consultar);
    }finally{
      dados_conexao.Desconectar();
      return resultado;
    }
  }
  public CachedRowSetImpl ConsultarPorTitulo(Livro livro) throws SQLException{
    dados_conexao = new Conexao();
    resultado = new CachedRowSetImpl();
    
    try{
      prepared_statement = dados_conexao.conexao_banco
              .prepareStatement("SELECT * FROM Livro " +
                                "WHERE titulo LIKE CONCAT('%', ?, '%') " +
                                "ORDER BY titulo");
      prepared_statement.setString(1, livro.getNome());
      
      resultado.populate(prepared_statement.executeQuery());
      
    }catch(SQLException erro_consultar){
      JOptionPane.showMessageDialog(null, 
              "Nao foi possivel consultar o livro." + 
              "\nErro: " + erro_consultar);
      
    }finally{
      dados_conexao.Desconectar();
      return resultado;
    }
  }
  
  public CachedRowSetImpl ConsultarPorEditora(Livro livro) throws SQLException{
    dados_conexao = new Conexao();
    resultado = new CachedRowSetImpl();
    
    try{
      prepared_statement = dados_conexao.conexao_banco
              .prepareStatement("SELECT * FROM Livro " +
                                "WHERE editora = ? " + 
                                "ORDER BY titulo");
      prepared_statement.setString(1, livro.getEditora());
      
      resultado.populate(prepared_statement.executeQuery());
      
    }catch(SQLException erro_consultar){
      JOptionPane.showMessageDialog(null, 
              "Nao foi possivel consultar o livro." + 
              "Erro: " + erro_consultar);
    }finally{
      dados_conexao.Desconectar();
      return resultado;
    }
  }
  public boolean Editar(Livro livro) throws SQLException{
    dados_conexao = new Conexao();
    
    try{
      prepared_statement = dados_conexao.conexao_banco
              .prepareStatement("UPDATE Livro " +
                                "SET titulo = ?, edicao = ?, editora = ?, " +
                                "cutter = ?, cod_CDU = ? " + 
                                "WHERE cod_livro = ?");
      prepared_statement.setString(1, livro.getNome());
      prepared_statement.setByte(2, livro.getEdicao());
      prepared_statement.setString(3, livro.getEditora());
      prepared_statement.setString(4, livro.getCutter());
      prepared_statement.setInt(5, livro.getCodCDU());
      prepared_statement.setInt(6, livro.getCodLivro());
      
      prepared_statement.executeUpdate();
      
      return true;
      
    }catch(SQLException erro_consultar){
      JOptionPane.showMessageDialog(null, 
              "Nao foi possivel editar o livro." +
              "\nErro: " + erro_consultar);
      return false;
      
    }finally{
      prepared_statement.close();
      dados_conexao.Desconectar();
    }
  }
}