/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Banco;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 *
 * @author vonn
 */
public class Conexao {
  String driver = "com.mysql.jdbc.Driver";
  String url = "jdbc:mysql://localhost:3306/grimm";
  String usuario = "root";
  String senha = "794613";
  
  private Connection conn = null;
  public Statement statement = null;
  public Connection conexao_banco = this.Conectar();
  
  
  public Connection Conectar(){
    try{
      Class.forName(driver);
      conn = DriverManager.getConnection(url, usuario, senha);
    }catch(ClassNotFoundException Driver){
      JOptionPane.showMessageDialog(null, "Driver não localizado: " + Driver);
    }catch(SQLException Fonte){
      JOptionPane.showMessageDialog(null, "Erro na conexão " +
              "com a fonte de dados: " + Fonte);
    }
    return conn;
  }

  public void Desconectar(){
    try{
      conn.close();
      //JOptionPane.showMessageDialog(null, 
      //        "Conexão com o bando de dados finalizada.");
    }catch(SQLException erro_fechar){
      JOptionPane.showMessageDialog(null, "Não foi possível " + 
              "fechar a conexão com o banco de dados: " + erro_fechar);
    }
  }
/*  public void ExecutarSQL(String consulta){
    try{
      statement = Conexao.createStatement(
              ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
      resultset = statement.executeQuery(consulta);
    }catch(SQLException erro_consultar){
      JOptionPane.showMessageDialog(null, "Não foi possível executar a " +
              "consulta \"" + consulta + "\".\n"
              + "Erro: " + erro_consultar);
    }
  }*/ 

}
