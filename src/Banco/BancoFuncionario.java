/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Banco;

import Classes.Funcionario;
import com.sun.rowset.CachedRowSetImpl;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author vonn
 */
public class BancoFuncionario extends BancoUsuario{
  
  // Metodo construtor
  public BancoFuncionario(){
    dados_conexao = null;
    prepared_statement = null;
    resultado = null;
  }
 
  
  
  
  // Metodos
  public boolean Cadastrar(Funcionario funcionario) throws SQLException{
    dados_conexao = new Conexao();
    
    try{
      prepared_statement = dados_conexao.conexao_banco
              .prepareStatement("INSERT INTO " +
              "Funcionario(cod_funcionario, login, senha, nivel) " +
              "VALUES (default, ?, ?, ?)");
      prepared_statement.setString(1, funcionario.getLogin());
      prepared_statement.setString(2, funcionario.getSenha());
      prepared_statement.setByte(3, funcionario.getFuncao());
      prepared_statement.executeUpdate();
      
      return true;
      
    }catch(SQLException erro_consultar){
      JOptionPane.showMessageDialog(null, "Nao foi possivel cadastrar o funcionario." + 
              "\nErro: " + erro_consultar);
      
      return false;
      
    }finally{
      dados_conexao.Desconectar();
      prepared_statement.close();
    }
  }
  
  public CachedRowSetImpl ConsultarPorCodigo(Funcionario funcionario) throws SQLException{
    dados_conexao = new Conexao();
    resultado = new CachedRowSetImpl();
    
    try{
      prepared_statement = dados_conexao.conexao_banco
              .prepareStatement("SELECT * FROM Funcionario " +
              "WHERE cod_funcionario = ?");
      prepared_statement.setInt(1, funcionario.getCod_funcionario());
      
      resultado.populate(prepared_statement.executeQuery());
      
    }catch(SQLException erro_consultar){
      JOptionPane.showMessageDialog(null, "Nao foi possivel consultar o funcionario." +
              "\nErro: " + erro_consultar);
      
    }finally{
      dados_conexao.Desconectar();
      return resultado;
    }
  }
}