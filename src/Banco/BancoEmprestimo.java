/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Banco;

import Classes.Emprestimo;
import Classes.Funcionario;
import Classes.Livro;
import Classes.Usuario;
import com.sun.rowset.CachedRowSetImpl;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author vonn
 */
public class BancoEmprestimo {
  Conexao dados_conexao;
  PreparedStatement prepared_statement;
  CachedRowSetImpl resultado;
  
  
  
  // Metodo construtor
  public BancoEmprestimo(){
    dados_conexao = null;
    prepared_statement = null;
    resultado = null;
  }
  
  
  
  // Metodos
  public boolean Criar(
          Emprestimo emprestimo, Usuario usuario, Livro livro, 
          Funcionario funcionario) throws SQLException{
    dados_conexao = new Conexao();
    
    try{
      prepared_statement = dados_conexao.conexao_banco
              .prepareStatement("INSERT INTO Emprestimo " +
              "(cod_emprestimo, cod_usuario, cod_livro, cod_funcionario, " +
              "data_emprestimo, data_devolucao, status) " +
              "VALUES (default, ?, ?, ?, ?, ?, ?)");
      prepared_statement.setInt(1, usuario.getCodUsuario());
      prepared_statement.setInt(2, livro.getCodLivro());
      prepared_statement.setInt(3, funcionario.getCod_funcionario());
      prepared_statement.setString(4, emprestimo.getData_emprestimo_());
      prepared_statement.setString(5, emprestimo.getData_devolucao_());
      prepared_statement.setBoolean(6, emprestimo.getStatus_emprestimo_());
      prepared_statement.executeUpdate();
      
      return true;
      
    }catch(SQLException erro_consultar){
      JOptionPane.showMessageDialog(null, "Nao foi possivel criar o emprestimo." +
              "\nErro: " + erro_consultar);
      
      return false;
      
    }finally{
      dados_conexao.Desconectar();
      prepared_statement.close();
    }
  }
  
  public CachedRowSetImpl ConsultarPorCodigo(Emprestimo emprestimo) throws SQLException{
    dados_conexao = new Conexao();
    resultado = new CachedRowSetImpl();
    
    try{
      prepared_statement = dados_conexao.conexao_banco
              .prepareStatement("SELECT * FROM Emprestimo " +
              "WHERE cod_emprestimo =  ?");
      prepared_statement.setInt(1, emprestimo.getCod_emprestimo_());
      
      resultado.populate(prepared_statement.executeQuery());
      
    }catch(SQLException erro_consultar){
      JOptionPane.showMessageDialog(null, "Nao foi possivel consultar o emprestimo." +
              "\nErro: " + erro_consultar);
      
    }finally{
      dados_conexao.Desconectar();
      return resultado;
    }
  }
  
  public CachedRowSetImpl ConsultarPorUsuario(Usuario usuario) throws SQLException{
    dados_conexao = new Conexao();
    resultado = new CachedRowSetImpl();
    
    try{
      prepared_statement = dados_conexao.conexao_banco
              .prepareStatement("SELECT * FROM Emprestimo " + 
              "WHERE cod_usuario = ?");
      prepared_statement.setInt(1, usuario.getCodUsuario());
      
      resultado.populate(prepared_statement.executeQuery());
     
    }catch(SQLException erro_consultar){
      JOptionPane.showMessageDialog(null, "Nao foi possivel consultar o emprestimo." + 
              erro_consultar);
      
    }finally{
      dados_conexao.Desconectar();
      return resultado;
    }
  }
  
  public boolean Editar(Emprestimo emprestimo, Usuario usuario, Livro livro, Funcionario funcionario) throws SQLException{
    dados_conexao = new Conexao();
    
    try{
      prepared_statement = dados_conexao.conexao_banco
              .prepareStatement("UPDATE Emprestimo " + 
              "SET cod_usuario = ?, cod_livro = ?, cod_funcionario = ?, data_emprestimo = ?, data_devolucao = ?, status = ? " +
              "WHERE cod_emprestimo = ?");
      prepared_statement.setInt(1, usuario.getCodUsuario());
      prepared_statement.setInt(2, livro.getCodLivro());
      prepared_statement.setInt(3, funcionario.getCod_funcionario());
      prepared_statement.setString(4, emprestimo.getData_emprestimo_());
      prepared_statement.setString(5, emprestimo.getData_devolucao_());
      prepared_statement.setBoolean(6, emprestimo.getStatus_emprestimo_());
      prepared_statement.setInt(7, emprestimo.getCod_emprestimo_());
      
      prepared_statement.executeUpdate();
      
      return true;
      
    }catch(SQLException erro_consultar){
      JOptionPane.showMessageDialog(null, "Nao foi possivel editar o emprestimo." + 
              "\nErro: " + erro_consultar);
      
      return false;
      
    }finally{
      dados_conexao.Desconectar();
      prepared_statement.close();
    }
  }
}
