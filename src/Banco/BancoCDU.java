/*  _      __     ___    _        ____     _____     ___
 *  (_    _) |    \  |  | \    ___)   )   (     )   (   
 *    |  |   |  |\ \ |  |  |  (__    /     \   /     \  
 *    |  |   |  | \ \|  |  |   __)  (       ) (       ) 
 *   _|  |_  |  |  \    |  |  (      \     /   \     /  
 *  (      )_|  |___\   |_/    \______)   (_____)   (___
 */

package Banco;

import Classes.CDU;
import com.sun.rowset.CachedRowSetImpl;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class BancoCDU {
  Conexao dados_conexao;
  PreparedStatement prepared_statement;
  CachedRowSetImpl resultado;
  
  
  
  // Metodo construtor
  public BancoCDU(){
    dados_conexao = null;
    prepared_statement = null;
    resultado = null;
  }
  
  
  
  // Metodos
  public boolean CadastrarCDU(CDU cdu) throws SQLException{
    dados_conexao = new Conexao();
    
    try{
      prepared_statement = dados_conexao.conexao_banco
              .prepareStatement("INSERT INTO CDU (cod_CDU, descricao) VALUES " + 
              "(?, ?)");
      prepared_statement.setInt(1, cdu.getCodCDU());
      prepared_statement.setString(2, cdu.getDescricao());
      prepared_statement.executeUpdate();
      
      return true;
      
    }catch(SQLException erro_consultar){
      JOptionPane.showMessageDialog(null, "Nao foi possivel cadastrar o CDU." + 
              "\nErro: " + erro_consultar);
      
      return false;
      
    }finally{
      dados_conexao.Desconectar();
      prepared_statement.close();
    }
  }
  
  public CachedRowSetImpl ConsultarCDU(CDU cdu)throws SQLException{
    dados_conexao = new Conexao();
    resultado = new CachedRowSetImpl();
    
    try{
      /*comando = dados_conexao.conexao_banco.createStatement(
              ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
      */
      prepared_statement = dados_conexao.conexao_banco
              .prepareStatement("SELECT * FROM CDU " + 
                                "WHERE cod_CDU = ?");
      prepared_statement.setInt(1, cdu.getCodCDU());
      
      //result_set = prepared_statement.executeQuery();
      resultado.populate(prepared_statement.executeQuery());
      
    }catch(SQLException erro_consultar){
      JOptionPane.showMessageDialog(null, "Nao foi possivel consultar o CDU." + 
              "\nErro: " + erro_consultar);
      
    }finally{
      dados_conexao.Desconectar();    
      return resultado;
    }
  }
  
  public boolean EditarCDU(CDU cdu) throws SQLException{
    dados_conexao = new Conexao();
    try{
      prepared_statement = dados_conexao.conexao_banco
              .prepareStatement("UPDATE CDU " + 
                                "SET descricao = ? " + 
                                "WHERE cod_CDU = ? ");
      prepared_statement.setString(1, cdu.getDescricao());
      prepared_statement.setInt(2, cdu.getCodCDU());
      
      prepared_statement.executeUpdate();
      
      return true;
      
    }catch(SQLException erro_consultar){
      JOptionPane.showMessageDialog(null, "Nao foi possivel editar o CDU." + 
              "\nErro: " + erro_consultar);
      
      return false;
      
    }finally{
      dados_conexao.Desconectar();
      prepared_statement.close();
    }
  }
}
