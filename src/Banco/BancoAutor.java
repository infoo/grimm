/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Banco;

import Classes.Autor;
import com.sun.rowset.CachedRowSetImpl;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author vonn
 */
public class BancoAutor {
  Conexao dados_conexao;
  PreparedStatement prepared_statement;
  CachedRowSetImpl resultado;
  
  
  
  // Metodo construtor
  public BancoAutor(){
    dados_conexao = null;
    prepared_statement = null;
    resultado = null;
  }
  
  
  
  // Metodos
  public boolean CadastrarAutor(Autor autor) throws SQLException{
    dados_conexao = new Conexao();
    
    try{
      prepared_statement = dados_conexao.conexao_banco
              .prepareStatement("INSERT INTO Autor (cod_autor, nome, nacionalidade) " + 
              "VALUES (default, ?, ?)");
      prepared_statement.setString(1, autor.getNome());
      prepared_statement.setString(2, autor.getNacionalidade());
      prepared_statement.executeUpdate();
      
      return true;
      
    }catch(SQLException erro_consultar){
      JOptionPane.showMessageDialog(null, "Nao foi possivel cadastrar o autor." +
              "\nErro: " + erro_consultar);
      
      return false;
      
    }finally{
      dados_conexao.Desconectar();
      prepared_statement.close();
    }
  }

  public CachedRowSetImpl ConsultarPorNome(Autor autor) throws SQLException{
      dados_conexao = new Conexao();
      resultado = new CachedRowSetImpl();
      
      try{
        prepared_statement = dados_conexao.conexao_banco
                .prepareStatement("SELECT * FROM Autor " +
                "WHERE nome LIKE CONCAT('%', ?, '%')");
        
        prepared_statement.setString(1, autor.getNome());
        
        resultado.populate(prepared_statement.executeQuery());
        
      }catch(SQLException erro_consultar){
        JOptionPane.showMessageDialog(null,
                "Nao foi possivel consultar o autor." +
                "\nErro: " + erro_consultar);
        
      }finally{
        dados_conexao.Desconectar();
        return resultado;
      }
    }
  
  public CachedRowSetImpl ConsultarPorNacionalidade(Autor autor) throws SQLException{
    dados_conexao = new Conexao();
    resultado = new CachedRowSetImpl();
    
    try{
      prepared_statement = dados_conexao.conexao_banco
              .prepareStatement("SELECT * FROM Autor " +
              "WHERE nacionalidade = ?");
      
      prepared_statement.setString(1, autor.getNacionalidade());
      
      resultado.populate(prepared_statement.executeQuery());
      
    }catch(SQLException erro_consultar){
      JOptionPane.showMessageDialog(null, 
              "Nao foi possivel consultar o autor." + 
              "\nErro: " + erro_consultar);
      
    }finally{
      dados_conexao.Desconectar();
      return resultado;
    }
  }  
}
