/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Banco;

import Classes.Usuario;
import com.sun.rowset.CachedRowSetImpl;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author vonn
 */
public class BancoUsuario {
  Conexao dados_conexao;
  PreparedStatement prepared_statement;
  CachedRowSetImpl resultado;
  
  
  
  // Metodo construtor
  public BancoUsuario(){
    dados_conexao = null;
    prepared_statement = null;
    resultado = null;
  }
  
  
  
  // Metodos
  public boolean Cadastrar(Usuario usuario) throws SQLException{
    dados_conexao = new Conexao();
    
    try{
      prepared_statement = dados_conexao.conexao_banco
              .prepareStatement("INSERT INTO Usuario " +
              "(cod_usuario, nome, RG, CPF, endereco, email) " +
              "VALUES (default, ?, ?, ?, ?, ?)");
      prepared_statement.setString(1, usuario.getNome());
      prepared_statement.setString(2, usuario.getRg());
      prepared_statement.setString(3, usuario.getCpf());
      prepared_statement.setString(4, usuario.getEndereco());
      prepared_statement.setString(5, usuario.getEmail());
      prepared_statement.executeUpdate();
      
      return true;
      
    }catch(SQLException erro_consultar){
      JOptionPane.showMessageDialog(null, "Nao foi possivel cadastrar o usuario." +
              "\nErro: " + erro_consultar);
      
      return false;
      
    }finally{
      dados_conexao.Desconectar();
      prepared_statement.close();
    }
  }
  
  public CachedRowSetImpl ConsultarPorCodigo(Usuario usuario) throws SQLException{
    dados_conexao = new Conexao();
    resultado = new CachedRowSetImpl();
    
    try{
      prepared_statement = dados_conexao.conexao_banco
              .prepareStatement("SELCT * FROM Usuario " +
              "WHERE cod_usuario = ?");
      prepared_statement.setInt(1, usuario.getCodUsuario());
      
      resultado.populate(prepared_statement.executeQuery());
      
    }catch(SQLException erro_consultar){
      JOptionPane.showMessageDialog(null, "Nao foi possivel consultar o usuario." + 
              "\nErro: " + erro_consultar);
      
    }finally{
      dados_conexao.Desconectar();
      return resultado;
    }
  }
 
  public CachedRowSetImpl ConsultarPorNome(Usuario usuario) throws SQLException{
    dados_conexao = new Conexao();
    resultado = new CachedRowSetImpl();
    
    try{
      prepared_statement = dados_conexao.conexao_banco
              .prepareStatement("SELECT * FROM Usuario " + 
              "WHERE nome LIKE CONCAT(%, ?, %)");
      prepared_statement.setString(1, usuario.getNome());
      
      
      resultado.populate(prepared_statement.executeQuery());
      
    }catch(SQLException erro_consultar){
      JOptionPane.showMessageDialog(null, "Nao foi possivel consultar o usuario." +
              "\nErro: " + erro_consultar);
      
    }finally{
      dados_conexao.Desconectar();
      return resultado;
    }
  }
  
  public CachedRowSetImpl ConsultarPorRG(Usuario usuario) throws SQLException{
    dados_conexao = new Conexao();
    resultado = new CachedRowSetImpl();
    
    try{
      prepared_statement = dados_conexao.conexao_banco
              .prepareStatement("SELECT * FROM Usuario " + 
              "WHERE RG = ?");
      prepared_statement.setString(1, usuario.getRg());
      
      resultado.populate(prepared_statement.executeQuery());
      
    }catch(SQLException erro_consultar){
      JOptionPane.showMessageDialog(null, "Nao foi possivel consultar o usuario." +
              "\nErro: " + erro_consultar);
      
    }finally{
      dados_conexao.Desconectar();
      return resultado;
    }
  }
  
  public boolean Editar(Usuario usuario) throws SQLException{
    dados_conexao = new Conexao();
    
    try{
      prepared_statement = dados_conexao.conexao_banco
              .prepareStatement("UPDATE Usuario " +
              "SET nome = ?, RG = ?, CPF = ?, endereco = ?, email = ? " + 
              "WHERE cod_usuario = ?");
      prepared_statement.setString(1, usuario.getNome());
      prepared_statement.setString(2, usuario.getRg());
      prepared_statement.setString(3, usuario.getCpf());
      prepared_statement.setString(4, usuario.getEndereco());
      prepared_statement.setString(5, usuario.getEmail());
      
      prepared_statement.executeUpdate();
      
      return true;
      
    }catch(SQLException erro_consultar){
      JOptionPane.showMessageDialog(null, "Nao foi possivel editar o usuario." +
              "\nErro: " + erro_consultar);
      
      return false;
      
    }finally{
      dados_conexao.Desconectar();
      prepared_statement.close();
    }
  }
}
